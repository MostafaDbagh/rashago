module web-auth

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0
	github.com/shaj13/go-guardian v1.5.11
)
