package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/shaj13/go-guardian/auth"
	"github.com/shaj13/go-guardian/auth/strategies/basic"
	"github.com/shaj13/go-guardian/auth/strategies/bearer"
	"github.com/shaj13/go-guardian/store"
)

var authenticator auth.Authenticator
var cache store.Cache

func main() {
	setupGoGuardian()
	router := gin.Default()
	router.GET("/v1/auth/token", createToken)
	router.GET("/v1/testurl/:id", getBookAuthor)

	router.Run(":8080")
}

func createToken(c *gin.Context) {
	token := uuid.New().String()
	user := auth.NewDefaultUser("medium", "1", nil, nil)
	tokenStrategy := authenticator.Strategy(bearer.CachedStrategyKey)
	auth.Append(tokenStrategy, token, user, c.Request)
	body := fmt.Sprintf("token: %s \n", token)
	c.Writer.Write([]byte(body))

}

func getBookAuthor(c *gin.Context) {

	id := c.Param("id")
	books := map[string]string{
		"1449311601": "Feras Darwish",
		"148425094X": "Mostafa Dbagh",
		"1484220498": "Rasha Ahmad",
	}
	body := fmt.Sprintf("Owner: %s \n", books[id])
	c.Writer.Write([]byte(body))
}

func setupGoGuardian() {
	authenticator = auth.New()
	cache = store.NewFIFO(context.Background(), time.Minute*10)

	basicStrategy := basic.New(validateUser, cache)
	tokenStrategy := bearer.New(bearer.NoOpAuthenticate, cache)

	authenticator.EnableStrategy(basic.StrategyKey, basicStrategy)
	authenticator.EnableStrategy(bearer.CachedStrategyKey, tokenStrategy)
}

func validateUser(ctx context.Context, r *http.Request, userName, password string) (auth.Info, error) {
	// here connect to db or any other service to fetch user and validate it.
	if userName == "mostafa" && password == "dbagh" {
		return auth.NewDefaultUser("medium", "1", nil, nil), nil
	}

	return nil, fmt.Errorf("Invalid credentials")
}
